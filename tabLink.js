function tabLink(id){
	this.id=id;
	this.links = [];
};

tabLink.prototype.getLinks = function() {
	return this.links;
};

tabLink.prototype.addLink = function(link) {
	if(this.links.indexOf(link)<0){
   	   this.links.push(link);
	}
};

tabLink.prototype.getLinksSize= function(){
	return this.links.length;
};

tabLink.prototype.deleteLinks= function(){
	this.links = []
};