var links = [];

function pushNotif(url) {
	chrome.notifications.create('reminder', {
		type: 'basic',
		iconUrl: '48.png',
		title: 'Video Link Found !',
		message: 'Please check extension icon'
	}, function (notificationId) { });
}

function checkAndSaveLink(url2, info) {
	var url = (url2 + "").toLowerCase();
	console.log(info)
	var tabId = info.tabId;
	if (!url.includes("tsyndicate") && containVideoSuffix(url)) {
		if (links[tabId.toString()] == null || links[tabId.toString()] == undefined) {
			links[tabId.toString()] = new tabLink(tabId);
		}
		links[tabId.toString()].addLink(url2);
	}
}

function containVideoSuffix(url) {
	videoFormat = [".mp4", ".flv", ".mkv", ".ts", ".webm"]
	for (let format of videoFormat) {
		if (url.includes(format)) {
			return true
		}
	}
	return false
}


var tablet = false;

chrome.webRequest.onHeadersReceived.addListener(function (info) {
	checkAndSaveLink(info.url, info);
}, { urls: ["<all_urls>"] }, ["responseHeaders"]);

chrome.webRequest.onBeforeSendHeaders
	.addListener(function (info) {
		//console.debug(info);
		checkAndSaveLink(info.url, info);
		// Replace the User-Agent header
		var headers = info.requestHeaders;
		headers
			.forEach(function (header, i) {
				if (tablet) {
					if (header.name.toLowerCase() == 'user-agent') {
						header.value = 'Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3';
					}
				}
			});
		return {
			requestHeaders: headers
		};
	},
		// Request filter
		{
			// Modify the headers for these pages
			urls: ["<all_urls>"]
		}, ["blocking", "requestHeaders"]);
chrome.tabs.onRemoved.addListener(function (tabId, removeInfo) {
	removeData(tabId);
	console.log(links);
});

chrome.tabs.onCreated.addListener(function (tab) {
	links[tab.id.toString()] = new tabLink(tab.id);
	console.log(links);
});

function removeData(tabId) {
	delete links[tabId.toString()];
}
