document.addEventListener('DOMContentLoaded', load, false);
function load(){

  chrome.tabs.query({active:true,windowType:"normal", currentWindow: true},function(d){
    var links = document.getElementById('links');
    var background = chrome.extension.getBackgroundPage();
    var linkArray = background.links;
    var tabId = d[0].id;
    tabIdDiv = document.createElement("div");
    tabIdDiv.id = "tabId"
    tabIdDiv.style = "display:none"
    tabIdDiv.innerHTML = tabId
    document.body.append(tabIdDiv)
    try{
      if(linkArray[tabId.toString()].getLinksSize()>0){
        for (var i = linkArray[tabId.toString()].getLinksSize()-1; i >= 0; i--) {
            var p = document.createElement("p");
            var div = document.createElement("div");
            var text = document.createTextNode(((i-(2*i))+linkArray[tabId.toString()].getLinksSize())+'.  '+linkArray[tabId.toString()].getLinks()[i]);

            p.setAttribute("id", 'content-' + i);
            p.prepend(text);
            div.innerHTML = '<a href="#" class="copy" data-content-id="content-' + i + '">Copy</a> | <a href="' + linkArray[tabId.toString()].getLinks()[i] + '" target="_blank">Download</a>';
            links.prepend(div);
            links.prepend(p);
        }
      }
    }catch(e){

    }

  });
}

function clearAll(e) {
  var links = document.getElementById('links');
  links.innerHTML = '';
  var background = chrome.extension.getBackgroundPage();
  var links = background.links;
  var tabIdDiv = document.getElementById('tabId');
  delete links[parseInt(tabIdDiv.innerHTML)]
}

function copyContent() {
  // console.log(el.getAttribute("data-content-id"));
}

var clear = document.getElementById('clear');
clear.addEventListener('click', clearAll);

// var elCopy = document.getElementsByClassName('copy');
// elCopy.addEventListener('click', copyContent(elCopy));
